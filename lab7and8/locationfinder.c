#pragma config(Sensor, S1,     Sonic,          sensorEV3_Ultrasonic)
#pragma config(Sensor, S2,     Gyro,           sensorEV3_Gyro, modeEV3Gyro_Rate)
#pragma config(Sensor, S3,     colour,         sensorEV3_Color)
#pragma config(Sensor, S4,     sonar4,         sensorEV3_Ultrasonic)
#pragma config(Motor,  motorB,          leftMotor,     tmotorEV3_Large, PIDControl, encoder)
#pragma config(Motor,  motorC,          rightMotor,    tmotorEV3_Large, PIDControl, encoder)

void gryo90()
{
	resetGyro(Gyro);

	setMotorSpeed(motorB, 50);
	setMotorSpeed(motorC, -50);
	waitUntil(getGyroDegrees(Gyro) > 80);

}
void oppgryo90()
{
	resetGyro(Gyro);

	setMotorSpeed(motorB, -50);
	setMotorSpeed(motorC, 50);
	waitUntil(getGyroDegrees(Gyro) < -80);

}

void drive(long nMotorRatio, long dist, long power)
{
	setMotorSyncEncoder(leftMotor, rightMotor, nMotorRatio, dist, power);

	//This won't allow the program to continue until the robot stops
	waitUntilMotorStop(motorB);
}

task main()
{
	int a;
	int dist;

	displayCenteredTextLine(1, "Pressed button:");
	displayCenteredBigTextLine(3, "Up for A(4,1)");
	displayCenteredBigTextLine(4, "Up for B(5,6)");
	
	// Loop forever
while(true)
{
		
		if (getButtonPress(buttonUp))
		{

			dist = 220;
			oppgryo90();
			drive(0, 220, 50);
			gryo90();
			drive(0, 660, 50);
			oppgryo90();
			drive(0, 440, 50);
			

			
			
			
		}
		else if (getButtonPress(buttonDown))
		{
			a = random(100);
			dist = 220;
			gryo90();
			drive(0, 220, 50);
			oppgryo90();
			drive(0, 220, 50);
			drive(0, 220, 50);
			drive(0, 220, 50);
			drive(0, 220, 50);
			gryo90();
			
	
			
		}
}
		



}